# MudPy

I've played MUD games for most of my life, and was tired of seeing unmaintained or ones that weren't actually multi-platform, or relied on so many unique dependencies that they were impossible to install.  MudPy is a pure Python implementation of a mud client.

The project is still in active development, but once it's at a stage where it's worth releasing I'll get it put on pypy.

![MudPy](/media/screenshot.png)
